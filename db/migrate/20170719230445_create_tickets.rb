class CreateTickets < ActiveRecord::Migration[5.1]
  def change
    create_table :tickets do |t|
      t.integer :origin, null: false
      t.integer :reason, null: false
      t.string :state, null: false
      t.string :notes, null: false

      t.timestamps
    end
  end
end
