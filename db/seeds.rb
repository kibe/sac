# ruby encoding: utf-8
first_creation_date  = DateTime.new(2017, 7, 21)
second_creation_date = DateTime.new(2017, 7, 20)

ticket_list = [
  [0, 1, :rj, "Teve uma otima postura e o atendimento foi bom", first_creation_date + 1.hour],
  [1, 2, :rn, "Acha que poderiamos ter enviado um doce.", first_creation_date + 1.hour ],
  [2, 1, :sp, "Belo atendimento", first_creation_date + 2.hour],
  [2, 1, :mg, "Foi ótimo", first_creation_date + 2.hour],
  [0, 2, :rn, "Adorei", second_creation_date + 1.hour],
  [1, 1, :mg, "Achei que poderia ter sido melhor", second_creation_date + 1.hour],
  [1, 2, :rj, "Razoavel", second_creation_date + 2.hour],
  [2, 0, :sp, "Teve uma otima postura e o atendimento foi bom", second_creation_date + 2.hour],
]

ticket_list.each do |type, reason, state, notes, created_at|
  Ticket.create(origin: type, reason: reason, state: state, notes: notes, created_at: created_at)
end
