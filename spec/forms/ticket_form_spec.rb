require 'rails_helper'

describe TicketForm, type: :model do
  describe 'Validations' do
    it { should validate_presence_of(:origin) }
    it { should validate_presence_of(:state) }
    it { should validate_presence_of(:reason) }
    it { should validate_presence_of(:notes) }
  end
end
