describe RetrieveTicketListGroupedByDateAndState do
  describe '.execute' do
    subject do
      described_class.execute
    end

    it 'returns grouped tickets when succeeded'do
      expect(subject).to have_key(:grouped_tickets)
      expect(subject).to have_key(:succeeded)
      expect(subject[:succeeded]).to be true
    end

    it 'expects grouped tickets to be grouped by state and date and ordered by date' do
      grouped_by = subject[:grouped_tickets].map{ |grouped_by, tickets| grouped_by }
      dates = grouped_by.map { |state, date| date.day }
      states = grouped_by.map { |state, date| state }
      expect(dates).to eq [21, 21, 21, 21, 20, 20, 20, 20]
      expect(states).to eq ["sp", "mg", "rj", "rn", "sp", "rj", "rn", "mg"]
    end
  end
end
