require 'rails_helper'

describe CreateTicket do
  describe 'when the attendant inputs everything accordingly' do
    subject do
      described_class.execute(origin: 0, state: :rj, reason: 0, notes: 'you shall pass.')
    end

    it do
      expect(subject).to have_key(:succeeded)
      expect(subject).to have_key(:errors)
      expect(subject).to have_key(:ticket)
      expect(subject[:succeeded]).to be true
      expect(subject[:errors]).to be_empty
    end
  end

  describe 'when the attendant forgets to write some info about the ticket' do
    subject do
      described_class.execute(origin: 0, state: nil, reason: 1, notes: 'you shall pass.')
    end

    it do
      expect(subject).to have_key(:succeeded)
      expect(subject).to have_key(:errors)
      expect(subject).to have_key(:ticket)
      expect(subject[:succeeded]).to be false
      expect(subject[:errors]).not_to be_empty
    end
  end
end
