# SAC :: Desafio OLX

O projeto utiliza o docker. para rodar:
```shell
# aqui ele levanta as maquinas, baixas as imagens para fazer o build dos containers.
docker-compose up --build

# cria o banco, migra e dá uma semeadinha (:)
docker-compose run --rm web bundle exec rake db:create db:migrate db:seed

# para rodar o servidor
docker-compose up

# Para rodar os testes:
docker-compose run --rm web bundle exec rspec
```

Muito obrigado pela oportunidade pessoal (:
:sunflower:
