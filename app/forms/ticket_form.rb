class TicketForm
  include ActiveModel::Model

  attr_accessor :origin, :reason, :state, :notes

  validates :origin, presence: true
  validates :reason, presence: true
  validates :state, presence: true
  validates :notes, presence: true
end
