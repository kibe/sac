class TicketsController < ApplicationController
  def index
    context = RetrieveTicketListGroupedByDateAndState.execute
    render locals: context
  end

  def new
  end

  def create
    context = CreateTicket.execute(permitted_params)
    if context[:succeeded]
      flash[:notice] = 'O ticket foi criado com sucesso'
      redirect_to action: :new
    else
      render :new, locals: context
    end
  end

  private

  def permitted_params
    params.require(:ticket).permit(:origin, :state, :reason, :notes)
  end
end
