class Ticket < ApplicationRecord
  # enum fields uses the array order
  enum origin: [:telephone, :chat, :email]
  enum reason: [:doubt, :compliment, :suggestion]
end
