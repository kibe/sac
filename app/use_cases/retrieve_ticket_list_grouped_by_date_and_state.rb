class RetrieveTicketListGroupedByDateAndState
  def self.execute
    new.execute
  end

  def execute
    retrieve_tickets
    group_tickets
    return_context
  end

  private

  def retrieve_tickets
    @tickets ||= Ticket.all.order(:created_at).reverse_order
  end

  def group_tickets
    @tickets = @tickets.group_by { |item| [item.state, item.created_at] }
  end

  def return_context
    {
      grouped_tickets: @tickets,
      succeeded: @tickets.present?
    }
  end
end
