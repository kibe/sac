class CreateTicket
  def self.execute(params)
    new(params).execute
  end

  def initialize(params)
    @params = params
  end

  def execute
    save_ticket
    context
  end

  private

  def ticket
    @ticket ||= Ticket.new(@params)
  end

  def ticket_form
    @form ||= TicketForm.new(@params)
  end

  def save_ticket
    ticket.save if ticket_form.valid?
  end

  def context
    {
      ticket: ticket,
      succeeded: ticket.persisted?,
      errors: ticket_form.errors
    }
  end
end
